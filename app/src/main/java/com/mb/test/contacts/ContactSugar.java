package com.mb.test.contacts;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.io.Serializable;

public class ContactSugar extends SugarRecord implements Serializable{

    @Unique
    @SerializedName("ContactNumber")
    public String ContactNumber;

    @Unique
    @SerializedName("contactid")
    public String contactid;

    @SerializedName("ContactImage")
    public String ContactImage;

    @SerializedName("Name")
    public String Name;

    @SerializedName("fav")
    public boolean fav = false;

    @SerializedName("del")
    public boolean del = false;


}
