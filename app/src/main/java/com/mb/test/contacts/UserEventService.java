package com.mb.test.contacts;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

public class UserEventService extends IntentService {

    private static final String ACTION_UPDATE_CONTACT = "ACTION_UPDATE_CONTACT";
    private static final String ACTION_DELETE_CONTACT = "ACTION_DELETE_CONTACT";
    private static final String EXTRA_CONTACT_UPDATE_OBJECT = "EXTRA_CONTACT_UPDATE_OBJECT";
    private static final String EXTRA_CONTACT_OBJECT = "EXTRA_CONTACT_OBJECT";

    public UserEventService() {
        super("UserEventService");
    }

    public static void updateContact(Context context, ContactSugar contactSugar) {
        Intent intent = new Intent(context, UserEventService.class);
        intent.setAction(ACTION_UPDATE_CONTACT);
        intent.putExtra(EXTRA_CONTACT_UPDATE_OBJECT, contactSugar);
        context.startService(intent);
    }

    public static void deleteContact(Context context, ContactSugar contactSugar) {
        Intent intent = new Intent(context, UserEventService.class);
        intent.setAction(ACTION_DELETE_CONTACT);
        intent.putExtra(EXTRA_CONTACT_OBJECT, contactSugar);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_CONTACT.equals(action)) {
                ContactSugar contactSugar = (ContactSugar) intent.getSerializableExtra(EXTRA_CONTACT_UPDATE_OBJECT);
                handleUpdateContact(contactSugar);
            } else if (ACTION_DELETE_CONTACT.equals(action)) {
                ContactSugar contactSugar = (ContactSugar) intent.getSerializableExtra(EXTRA_CONTACT_OBJECT);
                handledeleteContact(contactSugar);
            }
        }
    }

    private void handleUpdateContact(ContactSugar contactSugar) {
        Gson gson = new Gson();
        System.out.println("after deserialization");
        System.out.println(gson.toJson(contactSugar));

        contactSugar.save();
        EventBus.getDefault().post(new ContactListUpdateEvent());

    }

    private void handledeleteContact(ContactSugar contactSugar) {
        Gson gson = new Gson();
        System.out.println("after del deserialization");
        System.out.println(gson.toJson(contactSugar));
        contactSugar.save();
        EventBus.getDefault().post(new ContactListUpdateEvent());
    }

}
