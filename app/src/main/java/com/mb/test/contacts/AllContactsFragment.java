package com.mb.test.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orm.query.Condition;
import com.orm.query.Select;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllContactsFragment extends Fragment {

    public static final String POSITION = "position";
    public static final String CONTACT_ARRAYLIST = "contact_list";
    int position = 0;
    List<ContactSugar> contactSugarList = null;

    @BindView(R.id.rvContacts)
    RecyclerView rvContacts;

    @BindView(R.id.tv_no_record)
    TextView tv_no_record;

    private ContactsAdapter contactsAdapter;
    ContactFragmentType.FRAG_TYPE fragment_type_name = ContactFragmentType.FRAG_TYPE.valueOf(ContactFragmentType.FRAG_TYPE.FRAG_TYPE_CONTACTS.name());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            position = bundle.getInt(POSITION, 0);
            String status = bundle.getString(ContactFragmentType.FRAG_TYPE.class.getName(), ContactFragmentType.FRAG_TYPE.FRAG_TYPE_CONTACTS.name());
            fragment_type_name = ContactFragmentType.FRAG_TYPE.valueOf(status);
        }

        loadContacts(position);

        return view;
    }


    private void loadContacts(int position) {
        if (contactSugarList != null) {
            contactSugarList.clear();
        }

        if (position == 0) {
//            contactSugarList = Select.from(ContactSugar.class).list();
            contactSugarList = ContactSugar.listAll(ContactSugar.class);
        } else if (position == 1) {
            contactSugarList = Select.from(ContactSugar.class).where(Condition.prop("fav").eq("1")).list();
            tv_no_record.setText(getString(R.string.contacts_favorite_list_empty));
        } else if (position == 2) {
            contactSugarList = Select.from(ContactSugar.class).where(Condition.prop("del").eq("1")).list();
            tv_no_record.setText(getString(R.string.contacts_deleted_list_empty));
        }

        refreshAdapter(contactSugarList, fragment_type_name);
    }

    private void refreshAdapter(List<ContactSugar> contactSugarList,ContactFragmentType.FRAG_TYPE fragment_type_name) {
        contactsAdapter = new ContactsAdapter(getActivity(), contactSugarList, fragment_type_name);
        contactsAdapter.notifyDataSetChanged();

        if (contactSugarList.size() > 0) {
            tv_no_record.setVisibility(View.GONE);
            rvContacts.setVisibility(View.VISIBLE);
        } else {
            tv_no_record.setVisibility(View.VISIBLE);
            rvContacts.setVisibility(View.GONE);
        }

        rvContacts.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvContacts.setAdapter(contactsAdapter);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvContacts.getContext(), DividerItemDecoration.VERTICAL);
        rvContacts.addItemDecoration(mDividerItemDecoration);
        contactsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Receives event when order status will changed.
     * @param contactListUpdateEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactListUpdateEvent(ContactListUpdateEvent contactListUpdateEvent) {
        loadContacts(position);
    }
}
