package com.mb.test.contacts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.orm.query.Condition;
import com.orm.query.Select;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class FavoriteActivity extends AppCompatActivity {

    RecyclerView rvContacts;
    ContactsAdapter contactsAdapter;
    TextView tv_no_record;
    private ContactFragmentType.FRAG_TYPE frag_type = ContactFragmentType.FRAG_TYPE.valueOf(ContactFragmentType.FRAG_TYPE.FRAG_TYPE_FAVORITES.name());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);


        rvContacts = (RecyclerView) findViewById(R.id.rvContacts);
        tv_no_record = (TextView) findViewById(R.id.tv_no_record);

        loadContacts();
    }

    private void loadContacts() {
        List<ContactSugar> contactSugarList = Select.from(ContactSugar.class).where(Condition.prop("fav").eq("1")).list();
        System.out.println("contactSugarList = " +contactSugarList.size());

        if (contactSugarList.size() > 0 ) {
            tv_no_record.setVisibility(View.GONE);
            rvContacts.setVisibility(View.VISIBLE);
        } else {
            tv_no_record.setVisibility(View.VISIBLE);
            tv_no_record.setText(getString(R.string.contacts_favorite_list_empty));
            rvContacts.setVisibility(View.GONE);
        }

        contactsAdapter = new ContactsAdapter(FavoriteActivity.this, contactSugarList, frag_type);
        rvContacts.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvContacts.setAdapter(contactsAdapter);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvContacts.getContext(), DividerItemDecoration.VERTICAL);
        rvContacts.addItemDecoration(mDividerItemDecoration);
        contactsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Receives event when order status will changed.
     * @param contactListUpdateEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactListUpdateEvent(ContactListUpdateEvent contactListUpdateEvent) {
        loadContacts();
    }
}