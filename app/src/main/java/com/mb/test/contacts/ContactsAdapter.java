package com.mb.test.contacts;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private List<ContactSugar> contactsList;
    private Context mContext;
    //    private ContactFragmentType.FRAG_TYPE frag_type = null;
    private ContactFragmentType.FRAG_TYPE frag_type = ContactFragmentType.FRAG_TYPE.valueOf(ContactFragmentType.FRAG_TYPE.FRAG_TYPE_CONTACTS.name());

    ContactsAdapter(Context context, List<ContactSugar> contactsList) {
        this.mContext = context;
        this.contactsList = contactsList;
    }

    ContactsAdapter(Context context, List<ContactSugar> contactsList, ContactFragmentType.FRAG_TYPE fragment_type_name) {
        this.mContext = context;
        this.contactsList = contactsList;
        this.frag_type = fragment_type_name;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.contactlist_row, null);
        return new ViewHolder(view);
    }

    private void removeItem(int position, final boolean isRemoveOrAdded, ContactSugar contactSugar) {
        contactsList.get(position).del = isRemoveOrAdded;
        contactsList.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    private void updateData(int position, final boolean isAddedAsFavorite) {
        contactsList.get(position).fav = isAddedAsFavorite;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ContactSugar contacts = contactsList.get(position);
        String contactName = contacts.Name;
        final String contactNumber = contacts.ContactNumber;
        final String contactId = contacts.contactid;
        String contactImage = contacts.ContactImage;

        holder.tvContactName.setText(contactName);
        holder.tvPhoneNumber.setText(contactNumber);
        if (!TextUtils.isEmpty(contactImage)) {
            // make imageview visible and set textview visibility gone.
            // set Image URI Value.
            holder.ivCircleContactImage.setImageURI(Uri.parse(contactImage));
            holder.ivCircleContactImage.setVisibility(View.VISIBLE);
            holder.tvContactImage.setVisibility(View.GONE);
        } else {
            // Get First Letter and Make it Capital.
            // make textview visible and set image visibility gone.
            // set textview Value.
            holder.ivCircleContactImage.setVisibility(View.GONE);
            holder.tvContactImage.setVisibility(View.VISIBLE);
            String upperString = contactName.substring(0, 1).toUpperCase();
            holder.tvContactImage.setText(upperString);
        }

        if (contacts.fav) {
            holder.ivFavorite.setImageResource(R.drawable.ic_favorite_added);
        } else {
            holder.ivFavorite.setImageResource(R.drawable.ic_favorite_removed);
        }

        if (frag_type != null) {
            switch (frag_type) {
                case FRAG_TYPE_CONTACTS:
                    holder.rootView.setClickable(true);
                    holder.rootView.setEnabled(true);
                    holder.ivFavorite.setVisibility(View.VISIBLE);
                    holder.ivDelete.setVisibility(View.VISIBLE);
                    holder.ivDelete.setImageResource(R.drawable.ic_delete);
                    break;
                case FRAG_TYPE_FAVORITES:
                    holder.rootView.setClickable(true);
                    holder.rootView.setEnabled(true);
                    holder.ivDelete.setVisibility(View.GONE);
                    break;
                case FRAG_TYPE_DELETED:
                    holder.rootView.setClickable(false);
                    holder.rootView.setEnabled(false);
                    holder.ivFavorite.setVisibility(View.GONE);
                    holder.ivDelete.setVisibility(View.VISIBLE);
                    holder.ivDelete.setImageResource(R.drawable.ic_restore);
                    break;
            }
        }

        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Do your Yes progress
                        removeItem(position, !contacts.del, contacts);
//                        contacts.del = true;
                        UserEventService.deleteContact(mContext, contacts);
                        dialog.dismiss();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        //Do your No progress
                        break;
                }
            }
        };

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = mContext.getString(R.string.ask_for_delete);
                if (holder.ivDelete.getDrawable().getConstantState() == mContext.getResources().getDrawable(R.drawable.ic_delete).getConstantState()) {
                    msg = mContext.getString(R.string.ask_for_delete);
                } else {
                    msg = mContext.getString(R.string.ask_for_restore);
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(msg)
                        .setPositiveButton(mContext.getString(R.string.yes), dialogClickListener)
                        .setNegativeButton(mContext.getString(R.string.no), dialogClickListener)
                        .show();
            }
        });

        holder.ivFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData(position, !contacts.fav);
                UserEventService.updateContact(mContext, contacts);
            }
        });

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAlertDialogWithAction(mContext, contacts);
            }
        });
    }

    private void ShowAlertDialogWithAction(final Context context, final ContactSugar contactSugar) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.contact_action_popup_form, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder.setCancelable(true);
        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);

        Button btnCall = (Button) promptsView.findViewById(R.id.contact_action_popup_form_call_button);
        btnCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + contactSugar.ContactNumber));
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        Button btnSms = (Button) promptsView.findViewById(R.id.contact_action_popup_form_sms_button);
        btnSms.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Write code to open sms
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("sms:" + contactSugar.ContactNumber));
                intent.setType("vnd.android-dir/mms-sms");
                intent.putExtra("address", contactSugar.ContactNumber);
                intent.putExtra("sms_body", "Test Message");
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        // show it
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View rootView;
        public TextView tvContactName;
        public TextView tvPhoneNumber;
        public TextView tvContactImage;
        public CircleImageView ivCircleContactImage;
        public ImageView ivDelete;
        public ImageView ivFavorite;

        ViewHolder(View itemView) {
            super(itemView);
            this.setIsRecyclable(false);
            rootView = itemView;
            tvContactName = (TextView) itemView.findViewById(R.id.name);
            tvPhoneNumber = (TextView) itemView.findViewById(R.id.no);
            tvContactImage = (TextView) itemView.findViewById(R.id.tvContactImage);
            ivCircleContactImage = (CircleImageView) itemView.findViewById(R.id.ivCircleContactImage);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            ivFavorite = (ImageView) itemView.findViewById(R.id.ivFavorite);
        }
    }
}