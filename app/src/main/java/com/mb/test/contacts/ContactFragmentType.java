package com.mb.test.contacts;

public class ContactFragmentType {

    public enum FRAG_TYPE{
        FRAG_TYPE_CONTACTS,
        FRAG_TYPE_FAVORITES,
        FRAG_TYPE_DELETED
    }
}
