package com.mb.test.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FinalActivity extends FragmentActivity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
        ButterKnife.bind(FinalActivity.this);
        setAdapter();
    }

    private void setAdapter() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        viewPager.setAdapter(sectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        sectionsPagerAdapter.notifyDataSetChanged();
    }

    /**
     * Three Different types of Tab with Order status. and pass values in fragment using bundle.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Bundle bundle = new Bundle();
            ArrayList<ContactSugar> contactSugarArrayList = new ArrayList<>();

            AllContactsFragment allContactsFragment = new AllContactsFragment();
            switch (position) {
                case 0:
                    bundle.putString(ContactFragmentType.FRAG_TYPE.class.getName(), ContactFragmentType.FRAG_TYPE.FRAG_TYPE_CONTACTS.name());
                    bundle.putInt(AllContactsFragment.POSITION, 0);
                    break;
                case 1:
                    bundle.putString(ContactFragmentType.FRAG_TYPE.class.getName(), ContactFragmentType.FRAG_TYPE.FRAG_TYPE_FAVORITES.name());
                    bundle.putInt(AllContactsFragment.POSITION, 1);
                    break;
                case 2:
                    bundle.putString(ContactFragmentType.FRAG_TYPE.class.getName(), ContactFragmentType.FRAG_TYPE.FRAG_TYPE_DELETED.name());
                    bundle.putInt(AllContactsFragment.POSITION, 2);
                    break;
            }
            allContactsFragment.setArguments(bundle);
            return allContactsFragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return FinalActivity.this.getString(R.string.contacts);
                case 1:
                    return FinalActivity.this.getString(R.string.favorite);
                case 2:
                    return FinalActivity.this.getString(R.string.deleted);
            }
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Receives event when order status will changed.
     * @param contactListUpdateEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactListUpdateEvent(ContactListUpdateEvent contactListUpdateEvent) {
        setAdapter();
    }
}
