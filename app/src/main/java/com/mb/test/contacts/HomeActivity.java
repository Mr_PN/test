package com.mb.test.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    @BindView(R.id.btnSyncContact)
    Button btnSyncContact;

    @BindView(R.id.btnFavoriteContact)
    Button btnFavoriteContact;

    @BindView(R.id.btnDeletedContact)
    Button btnDeletedContact;

    @BindView(R.id.btnFinalContact)
    Button btnFinalContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        btnSyncContact.setOnClickListener(this);
        btnFavoriteContact.setOnClickListener(this);
        btnDeletedContact.setOnClickListener(this);
        btnFinalContact.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btnSyncContact:
                Intent intent = new Intent(this,ContactActivity.class);
                startActivity(intent);
                break;
            case R.id.btnFavoriteContact:
                Intent favoriteIntent = new Intent(this,FavoriteActivity.class);
                startActivity(favoriteIntent);
                break;
            case R.id.btnDeletedContact:
                Intent deleteIntent = new Intent(this,DeletedContactsActivity.class);
                startActivity(deleteIntent);
                break;
            case R.id.btnFinalContact:
                Intent finalIntent = new Intent(this,FinalActivity.class);
                startActivity(finalIntent);
                break;
        }
    }
}
